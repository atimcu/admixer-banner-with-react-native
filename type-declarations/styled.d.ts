import 'styled-components/native';

declare module 'styled-components/native' {
  export interface DefaultTheme {
    borderRadius: number;
    borderRadius16: number;
    categoryBoxShadow: string;
    mapControlBoxShadow: string;
    cardHeight: number;
    colors: {
      active: string;
      notActive: string;
      notActiveBorder: string;
      pressableLight: string;
      pressableDark: string;
      background: string;
      disabled: string;
      textButtonPrimary: string;
      bgButtonSecondary: string;
      textButtonSecondary: string;
      dividerColor: string;
      textColorSecondary: string;

      //NEW COLORS
      mapIconColor: string;
      mapWrapper: string;
      mapContainer: string;
      grayButton: string;
      blue: string;
      blue2: string;
      blue3: string;
      blue4: string;
      orange: string;
      red: string;
      redM: string;
      green: string;
      greenM: string;
      blueM: string;
      labelsColor: string;
      labelColor1: string;
      labelColor2: string;
      labelColor3: string;
      labelColor4: string;
      labelColor5: string;
      labelColorWhite: string;
      background1: string;
      background2: string;
      background3: string;
      background4: string;
      background5: string;
      background5WithoutOpacity: string;
      overlay: string;
      separator: string;
      separatorOpaque: string;
      systemFill1: string;
      systemFill2: string;
      systemFill3: string;
      systemFill4: string;
      alertBackground: string;
      pickerTimeBackground: string;
      errorBackground: string;
      bottomMenuBackground: string;
      pageBackground: string;
      cardBackground: string;
      inputPlaceholderColor: string;
      white: string;
      whiteOpaque: string;
      modalOverlay: string;
      blackOverlay: string;
      appleIconColor: string;
      appleBackgroundColor: string;
      simpalsLogoColor: string;
      transparent: string;
    };
    shadow: {
      shadow4px: string;
      shadowInverse4px: string;
    };
    screenPadding: string;
    boxShadow: string;
  }
}
