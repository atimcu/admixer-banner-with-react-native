/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

 import React from 'react';
 import {AdmixerBanner} from 'react-native-admixer';
 import {FlatList, SafeAreaView, Text, useColorScheme, View} from 'react-native';
 
 import {Colors} from 'react-native/Libraries/NewAppScreen';
 
  const data = [
    1, 2, 3, 4, 5
  ];
 
 var loadedIndex = 0;
 var failedIndex = 0;
 
 const CustomAdmixerBanner: React.FC = () => {
   return (
     <>
       <AdmixerBanner
         config={{
           zoneId: '058f671b-5347-4eae-bd83-3865909dcc35',
           bannerWidth: 300,
           bannerHeight: 250,
           sizes: [
             [300, 250],
             [320, 50],
           ],
         }}
         onAdLoaded={onAdLoaded}
         onAdLoadFailed={onAdLoadFailed}
       />
     </>
   );
 };
 
 class ListItem extends React.PureComponent {
 
   public render() {
     return (
       <View style={styles}>
         <Text> Start {this.props.item}</Text>
         <Text> Start Banner {this.props.item}</Text>
         <CustomAdmixerBanner />
         <Text> Finish Banner {this.props.item}</Text>
         <Text>Finish {this.props.item}</Text>
       </View>
     )
   }
 
 }
 
 function onAdLoaded() {
   console.log("MyCustomLog onAdLoaded "+loadedIndex);
   loadedIndex += 1;
 }
 
 function onAdLoadFailed() {
   console.log("MyCustomLog onAdLoadFailed " + failedIndex);
   failedIndex += 1;
 }
 
 const styles = {
   marginBottom: 50,
   marginTop: 50,
   borderWidth: 1,
   paddingLeft: 20,
   paddingRight: 20,
 };
 
 const App = () => {
   const isDarkMode = useColorScheme() === 'dark';
 
   const backgroundStyle = {
     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
   };
 
   return (
     <SafeAreaView style={backgroundStyle}>
       <FlatList
         data={data}
         // This method called twice without PureComponent
         renderItem={({item}) => {
           console.log("MyCustomLog renderItem "+item);
           return (
               <ListItem item={item}/>
           );
         }}
         // ItemSeparatorComponent={CustomAdmixerBanner}
       />
     </SafeAreaView>
   );
 };
 
 export default App;
 