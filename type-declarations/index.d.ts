declare module '*.png';
declare module 'sockjs-client';

interface AdmixerBannerProps {
  config: {
    zoneId: string;
    bannerWidth: number;
    bannerHeight: number;
    sizes: number[][];
  };
  onAdLoadFailed?: () => void;
  onAdLoaded?: () => void;
}

declare module 'react-native-admixer' {
  import React from 'react';

  class AdmixerBanner extends React.Component<AdmixerBannerProps> {}
}

declare module 'react-native-os-logger' {
  export function logDefault(str: string): void;
}
